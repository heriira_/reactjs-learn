import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
// import LifeCycle from './react_dasar/LifeCycle';
// import Map from './react_dasar/Map';
// import App from './App';
// import reportWebVitals from './reportWebVitals';
// import Variabel from './react_dasar/Variabel';
// import StateProps from './react_dasar/StateProps';
import 'bootstrap/dist/css/bootstrap.min.css';
import Crud from './crud'

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <Variabel/> */}
    {/* <StateProps/> */}
    {/* <Map/> */}
    {/* <LifeCycle/> */}
    <Crud/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();

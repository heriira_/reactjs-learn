import React, { Component } from "react";
import Formulir from "./Formulir";
import NavbarComponent from "./NavbarComponent";
import Tabel from "./Tabel";

export default class index extends Component {
  constructor(props) {
    super(props);

    this.state = {
      makanans: [],
      nama: "",
      harga: 0,
      deskripsi: "",
    };
  }

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();

    if (this.state.id === "") {
      this.setState({
        makanans: [
          ...this.state.makanans,
          {
            id: this.state.makanans.length + 1,
            nama: this.state.nama,
            deskripsi: this.state.deskripsi,
            harga: this.state.harga,
          },
        ],
      });
    } else {
      const makananSelainDipilih = this.state.makanans
        .filter((makanan) => makanan.id !== this.state.id)
        .map((filterMakanan) => {
          return filterMakanan;
        });

        this.setState({
          makanans: [
            ...makananSelainDipilih,
            {
              id: this.state.makanans.length + 1,
              nama: this.state.nama,
              deskripsi: this.state.deskripsi,
              harga: this.state.harga,
            },
          ],
        });
    }

    this.setState({
      nama: "",
      harga: 0,
      deskripsi: "",
    });
  };

  editData = (id) => {
    const makananDipilih = this.state.makanans
      .filter((makanan) => makanan.id === id)
      .map((filterMakanan) => {
        return filterMakanan;
      });

    this.setState({
      nama: makananDipilih[0].nama,
      harga: makananDipilih[0].harga,
      deskripsi: makananDipilih[0].deskripsi,
      id: makananDipilih[0].id,
    });
  };

  hapusData = (id) => {
    const makananBaru = this.state.makanans
    .filter((makanan) => makanan.id !== id)
    .map((filterMakanan) => {
      return filterMakanan;
    });

    this.setState({
      makanans : makananBaru
    })
  }

  render() {
    console.log(this.state.makanans);
    return (
      <div>
        <NavbarComponent />
        <div className="container mt-5">
          <Tabel makanans={this.state.makanans} editData={this.editData} hapusData={this.hapusData}/>
          <Formulir
            {...this.state}
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
          />
          {/* <h4 style={{ marginTop: "24px" }}>hi</h4> */}
        </div>
      </div>
    );
  }
}

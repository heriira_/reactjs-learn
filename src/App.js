import "./App.css";
import Body from "./Body";
import { Footer } from "./Footer";
import Header from "./Header";
import Navbar from "./Navbar";

function App() {
  return (
    <div className="App">
      <Navbar />
      <Header/>
      <Body/>
      <Footer/>
    </div>
  );
}

export default App;

import React, { Component } from "react";
import Sublifecycle from "./Sublifecycle";

export default class LifeCycle extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      makanan: "bakso",
      data: {},
      tampilhalamanSub: false,
    };
  }
  
  componentDidMount() {
    fetch("https://jsonplaceholder.typicode.com/todos/1")
      .then((response) => response.json())
      .then((json) => {
        //   console.log('hallo', json);
        this.setState({
          data: json,
        });
      });
  }

  ubahMakanan(value) {
    this.setState({
      makanan: value,
    });
  }

  render() {
    // console.log("Data : ", this.state.data);
    return (
      <div>
        <h4>{this.state.data.title}</h4>
        {this.state.tampilhalamanSub && (
          <Sublifecycle ubahMakanan={(value) => this.ubahMakanan(value)} />
        )}
        <button
          onClick={() =>
            this.setState({ tampilhalamanSub: !this.state.tampilhalamanSub })
          }
        >
          Tampilkan halaman sublifecycle
        </button>
      </div>
    );
  }
}

import React, { Component } from "react";

export default class Operan extends Component {
  render() {
    const { makanan, ganti_makanan } = this.props; //deklarasi this.props untuk mempersingkat coding
    return (
      <div>
        <h2>Operan state menjadi props : {makanan}</h2> {/* penangkapan props*/}
        <button onClick={() => ganti_makanan("bongko")}>ganti makanan</button> {/* penangkapan props*/}
      </div>
    );
  }
}

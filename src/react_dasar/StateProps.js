import React, { Component } from "react";
import Operan from "./Operan";

export default class StateProps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      makanan: "Bakso",
    };
  }

  // functional untuk setState 
  ganti_makanan = (makanan_baru) => {
    this.setState({
      makanan: makanan_baru,
    });
  }

  render() {
    return (
      <div>
        <h3>Menu kali ini adalah : {this.state.makanan}</h3>
        {/* <button onClick={() => this.setState({makanan : "Soto"})}> ganti makanan</button> */}
        <button onClick={() => this.ganti_makanan("bongko")}>
          ganti makanan
        </button>
        <Operan makanan={this.state.makanan} ganti_makanan={this.ganti_makanan}/> {/* Post state dan add fungsi ganti makanan*/}
      </div>
    );
  }
}

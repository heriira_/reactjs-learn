import React from "react";

const makanans = [
  {
    nama: "Baso",
    harga: 10000,
  },
  {
    nama: "Mie Ayam",
    harga: 14000,
  },
  {
    nama: "Soto",
    harga: 5000,
  },
  {
    nama: "Bongko",
    harga: 12000,
  },
];

// contoh Declare penggunaan reduce
const total_bayar = makanans.reduce((total_harga, makanan) => total_harga+makanan.harga, 0)

const Map = () => {
  return (
    <div>
      {/* Contoh penggunaan Map */}
      <h2>Map Sederhana</h2>
      {makanans.map((makanan, index) => (
        <li>{index + 1}.{makanan.nama} - harga {makanan.harga}</li>
      ))}

      {/* Contoh penggunaan Filter & Map */}
      <h2>Filter harga lebih dari 12.000</h2>
      {makanans
      .filter((makanan) => makanan.harga > 12000)
      .map((makanan, index) => (
        <li>{index + 1}.{makanan.nama} - harga {makanan.harga}</li>
      ))}

      {/* Contoh penggunaan Reduce */}
      <h2>Penggunaan Reduce</h2>
      <p>total bayar : {total_bayar}</p>
    </div>
    
  );
};

export default Map;

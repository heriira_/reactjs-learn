import React from "react";

/*Variabel Const tidak bisa di ubah2 valuenya */
const harga = 3000;

/*Variabel Var bersipat global props*/
// var harga = 50000;
// if (true) {
//   var harga = 101;
// }

/*Variabel Let bersipat block scope atau tidak bisa di akses di dalam suatu scope*/
// let harga = 10000;
// if (true) {
//   let harga = 200; //tidak bisa di akses dengan variabel lagi
//   harga = 200 //bisa di akses tanpa variabel
// }

const Variabel = () => {
  return (
    <div>
      <h2>harga : {harga}</h2>
    </div>
  );
};

export default Variabel;
